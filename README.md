# Advent of Code 2020
These are my attempts at the Advent of Code event for 2020.

See: [https://adventofcode.com/2020](https://adventofcode.com/2020)

## Run My Submissions
1. Have `go` installed.
2. Run `make`.

