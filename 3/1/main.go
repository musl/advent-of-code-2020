package main

import "fmt"

func main() {
	input := []string{
		`.#..........#...#...#..#.......`,
		`.###...#.#.##..###..#...#...#..`,
		`#.....#................#...#.#.`,
		`#.....#..###.............#....#`,
		`......#.....#....#...##.....###`,
		`....#........#.#......##....#.#`,
		`..#.......##..#.#.#............`,
		`#.............#..#...#.#...#...`,
		`.#...........#.#....#..##......`,
		`......#..##..#....#....#...##..`,
		`....#.##.#####..#.##..........#`,
		`..#.#......#.#.#....#.....#....`,
		`...###.##......#..#.#...#...#..`,
		`...#..#.#..#..#.......#........`,
		`...#....#..#...........#.#.....`,
		`....#.........###.#....#...#...`,
		`....#..##.....#.##....##.#.....`,
		`........#.#.#.....#........#...`,
		`..#..#.....#.#...#.#...#.#.....`,
		`....#..........#....#....#...##`,
		`.##...#..#...##....#..#.#....#.`,
		`.#....##..#...#................`,
		`..#.###.........#.###.....#....`,
		`....#..#.......###.#...........`,
		`#...#...#.#...........#.#......`,
		`.#..#.......##.....##...#......`,
		`....####.#..#.#.#...........#..`,
		`.##...#..#..#.#....##.....#..##`,
		`...#......##....#...#.#.###....`,
		`##.#...........#.........#...#.`,
		`...........#...#...........##..`,
		`.....#....#...........#........`,
		`...#..#.........#...#....#.##..`,
		`.....##.........#...#........##`,
		`....#....#..#.#...#...##.#.....`,
		`...#.#..#...#...........#..#...`,
		`.....#.#.....#....#...#....#...`,
		`.#.............#..##..........#`,
		`..........#......#..##.....###.`,
		`..#....#........#.#.....##...#.`,
		`#..#......#.#.##......#.#.##...`,
		`.....#..#.........#...#.#.#.#.#`,
		`#.#...#.......#.#..##.##.....##`,
		`.....#......##......#.......#..`,
		`#.....#...##.#.#........#......`,
		`#..........#.#...#.......#.....`,
		`..#..#........#........#.......`,
		`...#....#....#..####.#....#...#`,
		`#.............#.....##....#..#.`,
		`##....#.....###..##....#......#`,
		`#.....#...#.#.............#....`,
		`.#.#..##..##.#..#....#.#.#...#.`,
		`.#...#..#.....#..#.#.#..#...##.`,
		`..#.#.#.#.#.#....##...#........`,
		`.......##.....#..........#...#.`,
		`...#..#...#...........#....#...`,
		`.....#..#....#..#.##...#.......`,
		`..##..#.......#.#..#....#......`,
		`...#...............#.#..#......`,
		`....#........#...#....#...#.#..`,
		`...#...#..........##....##.#...`,
		`..###.#.##.............#..#.#.#`,
		`##.......##.#..#.#.#.....#.#.#.`,
		`..#####...#......##...#........`,
		`...#.##...#................#..#`,
		`..#......#...#....#.#..##..#...`,
		`#.#.........#............#.....`,
		`##.............#.#.....#......#`,
		`....#.......#..#..##....#.#....`,
		`...#...##....#.........#..#....`,
		`...####.....#...........#....#.`,
		`#.#........##....#..#..#...#...`,
		`....#.#.###..........#........#`,
		`#.#......#.....#.##....#.#...#.`,
		`#....##.#..##..#.#.............`,
		`.#.....##..#..................#`,
		`...#.#........#...#.#........#.`,
		`..#....#......#.....##........#`,
		`....#...#....#...#.....#.##....`,
		`...#........#.......##.........`,
		`.#.##......#......#....##......`,
		`.#...#...###.#............#..#.`,
		`.#...........#.#.#....#...#..#.`,
		`.#.....#....#.....#...#........`,
		`.#..#.....#............#.#.##.#`,
		`...###.#.............#..##.....`,
		`...#.#.##.#..#..........#..#...`,
		`.#.#.#....#..#...............##`,
		`.......#.#..#...#.#.#........#.`,
		`....#.#...#..##....#........#.#`,
		`..........#...#.......#..#....#`,
		`...###.....#.#....#.....##.....`,
		`#......#..#..#........#.#...#..`,
		`#......#....#..#.#.............`,
		`...#....#........#...#..#......`,
		`...#..###........#.#.........##`,
		`#......#.#..###..#........###..`,
		`.#.#......#.#..#.#.#.#.....#..#`,
		`#....#.....#..##.....#.........`,
		`....#......#...#..#..#.#.##.#..`,
		`........#.#...#...#..#...#.#..#`,
		`.....##........#...#....#...#..`,
		`....#...##..#........#....##.#.`,
		`...............#.....#......##.`,
		`..##.....#.....#.#.............`,
		`.....#.#...........##.#.....#..`,
		`.#..##..#.##.#...##.#....#....#`,
		`.##.....#.##......#....#..#..#.`,
		`.......#.##......#....#...#.#..`,
		`.#........#......#...##.#....#.`,
		`.........#..........#.......###`,
		`#.#.........#..#..#....#...#...`,
		`.......#.........#......#.#.#..`,
		`.......#...........#....#....#.`,
		`.###...##.#.#..........#...#..#`,
		`....#.....#...#..#.............`,
		`.......##........#..#.......#..`,
		`....##..#.#....#....#..#...#..#`,
		`..#.####.....#.........#.#....#`,
		`..............#.#..#.....#...#.`,
		`.....#.............#..........#`,
		`..##.#...#.....#....#.#....##..`,
		`.#...#.......#..####..#..#...#.`,
		`#..........#................##.`,
		`......##.....#.................`,
		`..##...#.#..........##.#...#...`,
		`....#.#.#.#...##...#...#...####`,
		`.............##..#.###...#.....`,
		`#.#....#.#..#..##........#..##.`,
		`.....#.#...............#.......`,
		`...#..##......#..##...........#`,
		`#..#....#...........##..#......`,
		`.##....#.#....###.......#..#...`,
		`.....#..#.#....##...#......#...`,
		`.#.........#####......#...#...#`,
		`.......#.#.....#.....#.......#.`,
		`#....#.......###.......#..#....`,
		`#......##.###...#.......#......`,
		`.......#...#......#....#..#....`,
		`.#.####.......#...#.##.........`,
		`................##.#......#....`,
		`......##....#.#......#......#..`,
		`....##...##....#.........#.....`,
		`......#.#..............##.#...#`,
		`....#.#......#.#.............#.`,
		`.#.#..####...#................#`,
		`....#.#.#.#......##...##......#`,
		`.....#.#..#......#....#......#.`,
		`..........#.#.....#.......#...#`,
		`..##......##.#...##.#......#..#`,
		`...#............#..#...###.....`,
		`.#.#..###..#.......##...#.....#`,
		`.#....#.#.......#.....##....#..`,
		`#.............###...##.#.#...#.`,
		`#........#.#........#.#...#.#.#`,
		`##..#.................#....#...`,
		`...#.#...#..#.#..##....#...#...`,
		`#.....#.......#..............#.`,
		`.......###...##..#.....#.......`,
		`#.#.........#..#.#.........#...`,
		`.#.#............#.....##.....#.`,
		`........#....#....#.......#....`,
		`...#.#....#..#.##....#.#......#`,
		`.#.....#.#..#...........#.#.#..`,
		`#......#..#......##.#.#.#.#..#.`,
		`.......#.#..#......#.#.#..#.#.#`,
		`..........#...#..........#.##..`,
		`.#.#..####.......#..........#..`,
		`......#.#.....#..#..#..#.....#.`,
		`.....##..#.#.#..#..#...#.....##`,
		`............#.#....#.#....#....`,
		`..............#..#...#...#.....`,
		`.....#......#.......#.....#....`,
		`..##....#..#...........#..##...`,
		`###...#.##..#.#...####....###..`,
		`..#.#.....#.........#....#..###`,
		`##...........##.............#..`,
		`....##..............#.........#`,
		`...#...##....#.#..#...##.....#.`,
		`..#..##...#.......#..#..#.....#`,
		`...#...#....####........##.#...`,
		`....#........#..#.#.........#..`,
		`.#..........#...#..#.#.#......#`,
		`....#.#.....#.........#....#...`,
		`...#....#...##.......#...#.....`,
		`....#..#.......#.##.##.##...#..`,
		`##....##........#........##....`,
		`.#.#..#...........#.....#...#..`,
		`...#.##...##..#...#...##.......`,
		`.....#..###................#.#.`,
		`...#........##.#....##.....#.##`,
		`...#...#..##...#...#.#...#.....`,
		`.#......#...#..#.##.......#...#`,
		`.....#.......###.##...#........`,
		`#.....#..#........##.##.#.##..#`,
		`....#..............##.##...#...`,
		`#..........#..................#`,
		`..##.......#..........#..#..##.`,
		`.#....###.#..#.........###....#`,
		`.#....#.##..............#.##.##`,
		`.#.##.#....#.......#.#......#..`,
		`.#............#.#.....#........`,
		`..#......#.......#.............`,
		`#.#...#........##...#.#......#.`,
		`....#.........#........##..#...`,
		`..........##.....#.#......#....`,
		`.##.#..#....#.......#...#...##.`,
		`.#................#...#.##.....`,
		`....###.......#..#..#.........#`,
		`.#.....#..##...###......#.....#`,
		`.#.##..........#..#..#........#`,
		`.......#.##..............#...##`,
		`#...#.#.#.......#..#......#.##.`,
		`.#....#.#......#...#..........#`,
		`.....#........##....#.##.....#.`,
		`.#....................#..#.#.#.`,
		`.....#.........#....#.......#.#`,
		`.....#.#..##..#.....#..#.......`,
		`...#..#..#...#.....#....#....#.`,
		`#.....#.#.#..........#..#.#.#..`,
		`.....##..##.....#.#..#.........`,
		`#.#..##....##......##...#.##..#`,
		`..##..#.....#..#..........##...`,
		`......#.#...#..#.......##.....#`,
		`..#.#.......#.#......#.........`,
		`.....#........##..#.....####.#.`,
		`.#.....#........#.......#..##..`,
		`......#...#....#.##...#.......#`,
		`..##..................#..#.....`,
		`.....###.#..##...#.............`,
		`...##...##...#......#....#....#`,
		`#........#.#..........##..#....`,
		`#........#....#..........#...#.`,
		`...##.#.##..#...##......#......`,
		`#........##....#.#..##.....#..#`,
		`...####......#..#......#.#.....`,
		`.#......#...#...#.#.....##....#`,
		`.....###..##..#...#..........##`,
		`##.##....#...#.................`,
		`...##.#.......#.###......#..#..`,
		`.....#.#.#.......#.......#..#.#`,
		`#...#...#.##..#....###.......#.`,
		`.#.#..##.....#....#...##.......`,
		`.....#..........#....#...#.##..`,
		`..........#....#...#...........`,
		`.#....#..#...#...#.......#....#`,
		`#..#..............#.....####.##`,
		`.......#....###....#....#.#.#..`,
		`###.#........##.#.......#......`,
		`#..#...#..#......#.............`,
		`#...###..#...#..#..##.#.###.#..`,
		`..#..#...##......##............`,
		`.#..#.......#..###..##...#.....`,
		`....#..#..##.#.#.....##...#.#.#`,
		`....#....#.....#..#....#.......`,
		`..##..#....#.#...##..#.........`,
		`.....#....#...........#.#......`,
		`...#........#.#..#..#......#..#`,
		`.#...##....#....#.#.##......#.#`,
		`..#...........#..###.##.....#..`,
		`.#.######.#..##.......#..#.....`,
		`.....#..#......##.#.#...#......`,
		`....#....#..#.....#.......#.#.#`,
		`.....#........##.....#.....#.##`,
		`........#....#...#...#.#.#...#.`,
		`...#.#.....#...........#.....#.`,
		`#.#.#...###......#.....#.....#.`,
		`.#..........#.....#.......##...`,
		`#................#.#.....#.####`,
		`.#......#......#.#..##.#.##....`,
		`..........#....#...........###.`,
		`.##....#..####..#####..........`,
		`##.......##............#.....#.`,
		`...#.....#...#....#.......#....`,
		`.#....##......#.#...#....#.....`,
		`....#............##..........#.`,
		`.#....#....#.....#.#...........`,
		`.............##.#.##...#.#.#...`,
		`..#............#.#..##.#....##.`,
		`#.....#...##..........#.#.#...#`,
		`......#............#..........#`,
		`..##..#.....#........#.##..#..#`,
		`#..#.#..##.#.....##.#..........`,
		`#..#...#.#..#......##.......##.`,
		`.##......#...........##.....#..`,
		`...#.....#.....#..#....#.......`,
		`.....#...............#........#`,
		`.......#.....##..#..##..#.#.#..`,
		`#.#.....#..#..........##...#...`,
		`#..#......#.................#.#`,
		`.##...#....#...#...#.......#...`,
		`.#........##........#..........`,
		`........#..........#.........#.`,
		`.....#.##..#.......#........#..`,
		`..##..#..#...##..#.#....#......`,
		`......#........#.##.....#.#....`,
		`.#...#.#.........#..#.#.#.#..#.`,
		`.#..#.#...#............#.#..#..`,
		`....#.................#...#..##`,
		`.........##.....#.#.#......####`,
		`...............#....##.#.#.....`,
		`....##..#....#......#....#.....`,
		`....##.#...#....#.#..#...#..#..`,
		`..##......#.#..#........#.#.#..`,
		`.........#.#................##.`,
		`##.....#.....##..##.#........#.`,
		`###....#..#..#..#..#.##..##.#..`,
		`.....##..#...........##..#.#...`,
		`....#..#..#..#....#...#.#....#.`,
		`#....#............#..#....###..`,
		`....#..#.............#....##.#.`,
		`...#.................#...#.....`,
		`.##...#....#..#..#........#....`,
		`...#.#..#...#.#......#....#....`,
		`...#.......##..........#...#.#.`,
		`...##..#.......#........#...#..`,
		`.....#.#.#....#..##......##...#`,
		`....##......#........##....##..`,
		`..#..........#.#.##.....#......`,
		`..................#..#..#..###.`,
		`.#..............#.#..#.#..#.###`,
		`..#....#....#......#..##..#...#`,
		`#.........#..#..#...........#..`,
	}

	treeSum := 0
	for i, l := range input {
		if l[i*3%len(l)] == byte('#') {
			treeSum++
		}
	}
	fmt.Println(treeSum)
}
