.PHONY: all echo

all: $(shell find . -name main.go | sort)

echo:
	@echo

%/main.go: echo
	@echo $@ | awk -F '/' '{print "Day "$$1" Exercize "$$2}'
	@echo '------------------------------------------------------------------------'
	@time go run -ldflags "-s -w" $@
	@echo
